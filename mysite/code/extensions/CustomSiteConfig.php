<?php

class CustomSiteConfig extends DataExtension{

	private static $db = array(
		'VideoLink' => 'Varchar(100)',

	);
	
	public function UpdateCMSFields(FieldList $fields){
		$fields->removeByName("Theme");

		$fields->addFieldsToTab("Root.Video", array(
			new TextField("VideoLink"),
		));
		
		return $fields;
	}
}