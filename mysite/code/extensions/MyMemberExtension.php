<?php

class MyMemberExtension extends DataExtension {
  /**

   * Modify the field set to be displayed in the CMS detail pop-up
   */
  public function updateCMSFields(FieldList $currentFields) {
    // Only show the additional fields on an appropriate kind of use 
    if(Permission::checkMember($this->owner->ID, "VIEW_FORUM")) {
      // Edit the FieldList passed, adding or removing fields as necessary
    }
  }

    // define additional properties
    private static $db = array(
      "Favourite" => "Int(10)",
      "Gender" => "Varchar(100)",
      "UserID" => "Varchar(100)",
      "Age" => "Int(10)",
      "UserName" => "Varchar(100)",
      "State" => "Varchar(100)"

      ); 

    private static $has_one = array(); 
    // private static $has_many = array(
    //   "Ecards" => "Ecards"
    //   ); 
    private static $many_many = array(); 
    private static $belongs_many_many = array(); 

  public function getCMSFields(){
      $fields = parent::getCMSFields();
    $fields->addFieldToTab('Root.Main', TextField::create("Favourite"));
return $fields;
  }

}