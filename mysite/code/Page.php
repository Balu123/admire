<?php
class Page extends SiteTree {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		'SearchForm'
	);

	public function init() {
		parent::init();
		
		// Requirements::combine_files(
		//     'ecards.js',
		//     array(
		//         'themes/ecards/js/jquerycad0.js',
		//         'themes/ecards/js/wpgroho3428.js',
		//         'themes/ecards/js/cart.min4468.js',
		//         'themes/ecards/js/main.min3428.js',
		//         'themes/ecards/js/facebook-embed.js',
		//         'themes/ecards/js/scripts.min3428.js',
		//         'themes/ecards/js/select2.mine248.js',
		//         'themes/ecards/js/wp-embed.min3428.js',
		//         'themes/ecards/js/add-to-cart.min4468.js',
		//         'themes/ecards/js/woocommerce.min4468.js',
		//         'themes/ecards/js/address-i18n.min4468.js',
		//         'themes/ecards/js/zxcvbn-async.min5152.js',
		//         'themes/ecards/js/comment-reply.min3428.js',
		//         'themes/ecards/js/jquery.cookie.min330a.js',
		//         'themes/ecards/js/lost-password.min4468.js',
		//         'themes/ecards/js/cart-fragments.min4468.js',
		//         'themes/ecards/js/country-select.min4468.js',
		//         'themes/ecards/js/jquery.blockUI.min44fd.js',
		//         'themes/ecards/js/jquery-migrate.min2fca.js',
		//         'themes/ecards/js/single-product.min4468.js',
		//         'themes/ecards/js/skip-link-focus-fix08e0.js',
		//         'themes/ecards/js/jquery.prettyPhoto.min005e.js',
		//         'themes/ecards/js/jquery.prettyPhoto.init.min4468.js',
		//         'themes/ecards/js/password-strength-meter.min4468.js',
		//        		)
		// );

		// Requirements::combine_files(
		// 		    'ecards.css',
		// 		    array(
		// 		        'themes/ecards/css/style3428.css',
		// 		        'themes/ecards/css/wc.min3428.css',
		// 		        'themes/ecards/css/jetpackfa0c.css',
		// 		        'themes/ecards/css/select24468.css',
		// 		        'themes/ecards/css/font-awesome.min3428.css',
		// 		        'themes/ecards/css/genericons128b.css',
		// 		        'themes/ecards/css/prettyPhoto4468.css',
		// 		        'themes/ecards/css/bootstrap.min68b3.css',
		// 		        'themes/ecards/css/woocommerce-layout4468.css',
		// 		        'themes/ecards/css/woocommerce-smallscreen4468.css',
		// 		       		)
		// 		);
	}

	public function Categories() {
		return Category::get();
	}			

	public function isBlocked() {
		// if($member = Member::currentUser()) {
        
	         $ipaddr = '';
			
    if (isset($_SERVER)) {

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            $ipaddr = $_SERVER["HTTP_X_FORWARDED_FOR"];

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
            $ipaddr = $_SERVER["HTTP_CLIENT_IP"];

        $ipaddr = $_SERVER["REMOTE_ADDR"];
    }

    if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddr = getenv('HTTP_X_FORWARDED_FOR');

    if (getenv('HTTP_CLIENT_IP'))
        $ipaddr = getenv('HTTP_CLIENT_IP');

			$blk = IpBlocker::get()->filter('IPAdress',$ipaddr)->first();
			if($blk) {
				if($blk->Block=='block')
				die('blocked');
			}
			
			return;
		
		// }
	}

	public function SearchForm() {
			// $searchText =  _t('SearchForm.SEARCH', 'Search');

			// if($this->owner->request && $this->owner->request->getVar('Search')) {
			// 	$searchText = $this->owner->request->getVar('Search');
			// }

			$searchField = TextField::Create('Search');//->setTitle('Search')->addExtraClass('form-control');

			// if($searchText && $searchText != 'Search') {
			// 	$searchField->setValue($searchText);
			// }
			
			$fields = new FieldList($searchField);

			$actions = new FieldList(
				new FormAction('results', _t('SearchForm.GO', 'Go'))
				);

			$form = new SearchForm($this, 'SearchForm', $fields, $actions);
			$form->addExtraClass('form-inline');

			
			$form->classesToSearch(array('SiteTree'));
			return $form;
		}	

	public function results($data, $form, $request) {	
		$keyword = trim($request->requestVar('Search'));
		$keyword = Convert::raw2sql($keyword);
		$keywordHTML = htmlentities($keyword, ENT_NOQUOTES, 'UTF-8');    
		$results = '';
		$ecards = '';
		if ($keywordHTML!='') {
			
		$results = Ecards::get()->filterAny('Name:PartialMatch', $keywordHTML);
		$cat = Category::get()->filter('Name', $keywordHTML)->first();
		if ($cat) {
		
		$cat = $cat->ID;
		$cat = Category::get()->byID($cat);
	   	$ecards = $cat->Ecards();

		}
				
		}
		$data = array(
			'Results' => $results,
			'Ecards' => $ecards,
			'Query' => $form->getSearchQuery(),
			'Title' => _t('SearchForm.SearchResults', 'Search Results')
			);

		return $this->customise($data)->renderWith(array('Page_results','Page'));
	}

	public function VidPopup() {
		$Month = 2592000 + time();
		setcookie('pop-video', true, $Month);
		ob_get_clean();

	 	if(!isset($_COOKIE['pop-video'])) {
	 		return true;
	 	} else {
	 		return false;
	 	}

	}
																																																																																											
}
