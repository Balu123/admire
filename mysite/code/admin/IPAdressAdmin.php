<?php
class IPAdressAdmin extends ModelAdmin {
    
    private static $menu_title = 'ipadresshandler';

    private static $url_segment = 'ipadresshandler';

    private static $managed_models = array (
        'IPBlocker',
    );
}