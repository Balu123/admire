<?php
class Ecard extends ModelAdmin {

 	private static $menu_icon = 'themes/ecards/icons/gift-16.png';
    
    private static $menu_title = 'Ecards';

    private static $url_segment = 'Ecards';

    private static $managed_models = array (
        'Ecards',
        'Category',
        'Skins',
        'Stamps',
        'Music',
        'Poem'

    );
}