<?php

class RegistrationPage extends Page {

	public static $db = array(
		'RegistratinSuccessContent' => "HTMLText",
		'SendMailAfterRegistration' => 'Boolean',
		'RegistrationEmailSubject'	=> 'Varchar',
		'RegistrationEmailTemplate'	=> "HTMLText"
	);

	public function canCreate($member = null){
		return !RegistrationPage::get()->first();
	}

	public function canDelete($member = null){
		return null;
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->addFieldsToTab("Root.Main", array(
			HTMLEditorField::Create("RegistratinSuccessContent")->SetTitle("Registratin Success Page Content")->setRightTitle("Available Placeholders '#Title' '#FirstName' '#LastName' ")
		));

		$fields->addFieldsToTab("Root.EmailSettings", array(
			new CheckboxField("SendMailAfterRegistration", "Send Mail After Registration?"),
			new TextField("RegistrationEmailSubject", "Email Subject"),
			HTMLEditorField::Create("RegistrationEmailTemplate")->SetTitle("Email Content")->setRightTitle("Available Placeholders '#Title' '#FirstName' '#LastName' ")
		));

		return $fields;
	}
}

class RegistrationPage_Controller extends Page_Controller {

	private static $allowed_actions = array (
		'RegistrationForm',
		'complete'
		);

	public function init() {
		parent::init();
	}

	public function RegistrationForm() {
    $states = array(
        'Bedfordshire' => 'Bedfordshire',
        'Berkshire' => 'Berkshire',
        'Buckinghamshire' => 'Buckinghamshire',
        'Cambridgeshire' => 'Cambridgeshire',
        'Cheshire' => 'Cheshire',
        'Cornwall' => 'Cornwall',
        'Cumberland' => 'Cumberland',
        'Derbyshire' => 'Derbyshire',
        'Devon' => 'Devon',
        'Dorset' => 'Dorset',
        'Durham' => 'Durham',
        'East Yorkshire' => 'East Yorkshire',
        'Essex' => 'Essex',
        'Gloucestershire' => 'Gloucestershire',
        'Hampshire' => 'Hampshire',
        'Herefordshire' => 'Herefordshire',
        'Hertfordshire' => 'Hertfordshire',
        'Huntingdonshire' => 'Huntingdonshire',
        'Kent' => 'Kent',
        'Lancashire' => 'Lancashire',
        'Leicestershire' => 'Leicestershire',
        'Lincolnshire' => 'Lincolnshire',
        'Middlesex' => 'Middlesex',
        'Norfolk' => 'Norfolk',
        'North Yorkshire' => 'North Yorkshire',
        'Northamptonshire' => 'Northamptonshire',
        'Northumberland' => 'Northumberland',
        'Nottinghamshire' => 'Nottinghamshire',
        'Oxfordshire' => 'Oxfordshire',
        'Rutland' => 'Rutland',
        'Shropshire' => 'Shropshire',
        'Somerset' => 'Somerset',
        'Staffordshire' => 'Staffordshire',
        'Suffolk' => 'Suffolk',
        'Surrey' => 'Surrey',
        'Sussex' => 'Sussex',
        'Warwickshire' => 'Warwickshire',
        'West Yorkshire' => 'West Yorkshire',
        'Westmorland' => 'Westmorland',
        'Wiltshire' => 'Wiltshire',
        'Worcestershire' => 'Worcestershire',
        'Aberdeenshire' => 'Aberdeenshire',
        'Angus/Forfarshire' => 'Angus/Forfarshire',
        'Argyllshire' => 'Argyllshire',
        'Ayrshire' => 'Ayrshire',
        'Banffshire' => 'Banffshire',
        'Berwickshire' => 'Berwickshire',
        'Buteshire' => 'Buteshire',
        'Cromartyshire' => 'Cromartyshire',
        'Caithness' => 'Caithness',
        'Clackmannanshire' => 'Clackmannanshire',
        'Dumfriesshire' => 'Dumfriesshire',
        'Dunbartonshire/Dumbartonshire' => 'Dunbartonshire/Dumbartonshire',
        'East Lothian/Haddingtonshire' => 'East Lothian/Haddingtonshire',
        'Fife' => 'Fife',
        'Inverness-shire' => 'Inverness-shire',
        'Kincardineshire' => 'Kincardineshire',
        'Kinross-shire' => 'Kinross-shire',
        'Kirkcudbrightshire' => 'Kirkcudbrightshire',
        'Lanarkshire' => 'Lanarkshire',
        'Midlothian/Edinburghshire' => 'Midlothian/Edinburghshire',
        'Morayshire' => 'Morayshire',
        'Nairnshire' => 'Nairnshire',
        'Orkney' => 'Orkney',
        'Peeblesshire' => 'Peeblesshire',
        'Perthshire' => 'Perthshire',
        'Renfrewshire' => 'Renfrewshire',
        'Ross-shire' => 'Ross-shire',
        'Roxburghshire' => 'Roxburghshire',
        'Selkirkshire' => 'Selkirkshire',
        'Shetland' => 'Shetland',
        'Stirlingshire' => 'Stirlingshire',
        'Sutherland' => 'Sutherland',
        'West Lothian/Linlithgowshire' => 'West Lothian/Linlithgowshire',
        'Wigtownshire' => 'Wigtownshire',
        'Anglesey/Sir Fon' => 'Anglesey/Sir Fon',
        'Brecknockshire/Sir Frycheiniog' => 'Brecknockshire/Sir Frycheiniog',
        'Caernarfonshire/Sir Gaernarfon' => 'Caernarfonshire/Sir Gaernarfon',
        'Carmarthenshire/Sir Gaerfyrddin' => 'Carmarthenshire/Sir Gaerfyrddin',
        'Cardiganshire/Ceredigion' => 'Cardiganshire/Ceredigion',
        'Denbighshire/Sir Ddinbych' => 'Denbighshire/Sir Ddinbych',
        'Flintshire/Sir Fflint' => 'Flintshire/Sir Fflint',
        'Glamorgan/Morgannwg' => 'Glamorgan/Morgannwg',
        'Merioneth/Meirionnydd' => 'Merioneth/Meirionnydd',
        'Monmouthshire/Sir Fynwy' => 'Monmouthshire/Sir Fynwy',
        'Montgomeryshire/Sir Drefaldwyn' => 'Montgomeryshire/Sir Drefaldwyn',
        'Pembrokeshire/Sir Benfro' => 'Pembrokeshire/Sir Benfro',
        'Radnorshire/Sir Faesyfed' => 'Radnorshire/Sir Faesyfed',
        'County Antrim' => 'County Antrim',
        'County Armagh' => 'County Armagh',
        'County Down' => 'County Down',
        'County Fermanagh' => 'County Fermanagh',
        'County Tyrone' => 'County Tyrone',
        'County Londonderry/Derry' => 'County Londonderry/Derry',
    );
	$age = array();
	for ($i=1; $i < 90; $i++) { 
		$age[$i] = $i;
		$i++;
	}
		
		$fields = new FieldList(
			
		
			TextField::Create('FirstName')->SetTitle('Name <span>*</span>'),
			
			DropdownField::Create('Age', 'Age', $age)->SetTitle('Age <span>*</span>'),
            
            OptionsetField::create('Gender', 'Gender', array(
                    'Male' => 'Male',
                    'Female' => 'Female'
                ),$value = "Male")->SetTitle('Gender <span>*</span>'),
            
            TextField::Create('UserID')->SetTitle('UserID <span>*</span>'),
            
            TextField::Create('UserName')->SetTitle('UserName
            UserName <span>*</span>'),
			
            EmailField::Create('Email')->SetTitle('Email Address <span>*</span>'),
                        
			
			ConfirmedPasswordField::Create('Password')->SetTitle(''),

            DropdownField::Create('State', 'State', $states)->SetTitle('State <span>*</span>'));


		$actions = new FieldList(
			new FormAction('doRegister', 'Register')
			);

		$validator = new RequiredFields('FirstName', 'LastName', 'Email', 'Password');

		return new Form($this, 'RegistrationForm', $fields, $actions, $validator); 
	}

	public function doRegister($data,$form) {

        if($member = Member::get("Member")->filter("Email" , Convert::raw2sql($data['Email']))->first()) {
            $form->AddErrorMessage('Email', "Sorry, that email address already exists. Please choose another.", 'bad');
            Session::set("FormInfo.Form_RegistrationForm.data", $data);     
            return $this->redirectBack();;           
        }   
 
        $Member = new Member();
        $ip = new IPBlocker();
        $form->saveInto($Member);            
        $Member->write();
        $ipaddr = $_SERVER['REMOTE_ADDR'];
        $ip->UserName = $data['FirstName'];
        $ip->IPAdress = $ipaddr;
        $ip->write();

        $Member->login();
         
        if(!$userGroup = Group::get()->filter("Code" , "dyamembers")->first()) {
            $userGroup = new Group();
            $userGroup->Code = "dyamembers";
            $userGroup->Title = "DYAMembers";
            $userGroup->Write();
            $userGroup->Members()->add($Member);
        }
        
        $userGroup->Members()->add($Member);

        if($this->SendMailAfterRegistration) {
        	$this->SendMail($Member);
        }

        Session::set('RegisteredMemberID', $Member->ID);

        $this->redirect($this->Link('complete'));

	}

	public function SendMail($Member) {
		$strContent = $this->RegistratinSuccessContent;

		$strContent = str_replace('#Title', $Member->NameTitle ,$strContent);
		$strContent = str_replace('#FirstName', $Member->FirstName ,$strContent);
		$strContent = str_replace('#LastName', $Member->Surname ,$strContent);

		$e = new Email();
		$e->To = $Member->Email;
		$e->Subject = $this->RegistrationEmailSubject;
		$e->Body = $strContent;
		$e->send();
	}

	public function complete() {
		$strContent = $this->RegistratinSuccessContent;
		$iRegisteredMemberID = Session::get('RegisteredMemberID');
		
		if($iRegisteredMemberID) {
			$doMember = Member::get()->byID($iRegisteredMemberID);

			if(is_object($doMember)) {
				$strContent = str_replace('#Title', $doMember->NameTitle ,$strContent);
				$strContent = str_replace('#FirstName',$doMember->FirstName ,$strContent);
				$strContent = str_replace('#LastName', $doMember->Surname ,$strContent);

				Session::clear('RegisteredMemberID');

				return array("Title" => 'Registration Complete', "Content" => $strContent);
			}

		} else {
			$this->redirect($this->Link());
		}		
	}
}
