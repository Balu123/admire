<?php

class SendCardPage extends Page {

}

class SendCardPage_Controller extends Page_Controller {

	private static $allowed_actions = array (
		'SendCard'
		);

	public function SendCard() {
		$sname = $this->request->requestVar('s_name');
		$semail = $this->request->requestVar('s_email');
		$rname = $this->request->requestVar('r_name');
		$remail = $this->request->requestVar('r_email');
		$subject = $this->request->requestVar('subject');
		$message = $this->request->requestVar('editor1');
		
	    Session::set('Message', array('sname'=>$sname,'semail'=>$semail,'rname'=>$rname, 'remail' => $remail,'subject' => $subject, 'message' => $message));
		

		return $this->redirect( Director::baseURL() . "ecard-submission" );
	}

}