<?php

class EcardsHolder extends Page {

}

class EcardsHolder_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
        'Category',
        'Show',
        'view'
    );

    public function Category(SS_HTTPRequest $request) {
    	
	    $id = $request->param('ID');
	    $q = $request->getVar('orderby');

	    if($q=="price-desc") {
	    	$cat = Category::get()->byID($id);
	    	$ecards = $cat->Ecards()->sort('Name', 'DESC');
	    } else {
	    	$cat = Category::get()->byID($id);
	    	$title = $cat->Name;
	   	 	$ecards = $cat->Ecards();
	    }

	    if(!$ecards) {
	        return $this->httpError(404,'No Ecards found in this category');
	    }

	    return array (
	        'Ecards' => $ecards,
	        'Title' => $title
	    );
    }

    public function Show(SS_HTTPRequest $request) {

	    	$id = $request->param('ID');
    		
    		$isFav = $this->favourite($id);

	    	$ecard = Ecards::get()->byID($id);
	    	$str = substr($ecard->Name,0);
	    	$rel = Ecards::get()->filter(array(
			    'Name:StartsWith' => $str
			))->exclude('Name', $ecard->Name);

	    if(!$ecard) {
	        return $this->httpError(404,'No Ecards found in this category');
	    }
	    $skins = Skins::get();
	    $poems = Poem::get();
	    $stamps = Stamps::get();
	    $music = Music::get();
	    
	    $mus = '';

	    if ($singmusic = $request->getVar('music')) {
	    	$mus = Music::get()->byID($singmusic);
	    	Session::set('music', $singmusic);
	    } else {
	    	$mus= Music::get()->byID(Session::get('music'));
	    }
	    $p = '';

		if ($singpoem = $request->getVar('poem')) {
			    	$p = Poem::get()->byID($singpoem);
	    			Session::set('poem', $singpoem);

		} else {
			    	$p = Poem::get()->byID(Session::get('poem'));
		}
	   
	   	$ski = '';

	   	 if ($singskin = $request->getVar('skins')) {
	    	$ski = Skins::get()->byID($singskin);
	    	Session::set('skin', $singskin);
	    } else {
	    	$ski = Skins::get()->byID(Session::get('skin'));
	    }

	   	$stmp = '';

	    if ($stamp = $request->getVar('stamp')) {
	    	$stmp = Stamps::get()->byID($stamp);
	    	Session::set('stamp', $stamp);
	    } else {
	    	$stmp = Stamps::get()->byID(Session::get('stamp'));
	    }
	

	    Session::set('MyArrayOfValues', array('EcardID'=>$id,'MusicID'=>$singmusic,'PoemID'=>$singpoem, 'SkinID' => $singskin,'StampID' => $stamp));
	    
	    return array (
	        'Ecard' => $ecard,
	        'Title' => $ecard->Name,
	        'RelatedEcards' => $rel,
	        'Skins' => $skins,
	        'Ski' => $ski ,
	        'SingPoem' => $p,
	        'Poems' => $poems,
	        'Music' => $music,
	        'Stamps' => $stamps,
	        'SingMusic' => $mus,
	        'Stamp' => $stmp,
	        'IsFav' => $isFav

	    );
    }

    public function Ecards() {
    	Session::clear('skin');
		Session::clear('music');
		Session::clear('poem');
		Session::clear('stamp');
    	return Ecards::get();
    }

// public function view() {
// 		$link = $this->request->getVar('id');
//     	die("view Card here <a href=>$link</a>");
//     }


	public function favourite($id) {

				if ( $member = Member::currentUser() ) {
				$e = Ecards::get()->byID($id);
				$favo = Fav::get()->filter(array("UserID"=>$member->ID,"EcardID"=>$id))->first();
                $fav = $this->request->requestVar('fav');
                $delfav = $this->request->requestVar('delfav');
				if( $fav ) {
				    if($favo)
				    return "yes";	
				    $f = new Fav();
				
				    
				    $f->UserID = $member->ID;
				    $f->EcardID = $id;

				    $f->write();	
				    return "yes";
				    
				    } elseif( $delfav ) {
				    	
				    			$d = Fav::get()->filter(array("UserID"=>$member->ID,"EcardID"=>$id))->first();
				    		
								foreach ($d as $item) {
									$item->delete();
								}
								return "no";
				    		} else {
                                if ($favo) {
                                    return "yes";
                                }
                                // die('works');
				    			return "no";
				    		}
			} else {

				return "no";

			}
	}

} 