<?php

class EcardSubmission extends Page {

	private static $has_many = array(
		"SentEcard" => "SentEcard"
		);
}

class EcardSubmission_Controller extends Page_Controller {

	private static $allowed_actions = array ();


	public function Ecards() {
		return Ecards::get();
	}

	public function EcardSubmit() {
		$length = rand(10,50);
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';

	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    if(!empty(Session::get('MyArrayOfValues'))) {
			$es = new SentEcard();	 
			$arr = Session::get('MyArrayOfValues');
		   	$skin = Session::get('skin');
		   	$poem = Session::get('poem');
		   	$music = Session::get('music');
		   	$stamp = Session::get('stamp');

		   	if (!empty($arr['EcardID'])) {
				$es->EcardID = $arr['EcardID'];
		   	}

		   	if (!empty($music)) {
				$es->MusicID = $music;
		   	}

		   	if (!empty($skin)) {
				$es->SkinID = $skin;	   		
		   	}

		   	if (!empty($stamp)) {
				$es->StampID = $stamp;	   		
		   	}

		   	if (!empty($poem)) {
				$es->PoemID = $poem;
		   	 } 

			$es->UID = $randomString; 

			$es->write();
			Session::clear('skin');
			Session::clear('music');
			Session::clear('poem');
			Session::clear('stamp');

		   	$data = Session::get('Message');

			$From = $data['semail'];
			$To = $data['remail'];
			$Subject = $data['subject'];
			$Body = $data['message'];
			$link = Director::baseURL()."ecard-submission?card-id=".$es->UID;
			$Body .= $data['sname']." Sent you an Ecard from doyouadmire "; 
			$Body .= "Here is the link <a href=".$link.">Link</a>";
			$email = new Email($From, $To, $Subject, $Body);
			$email->send();	
			
			Session::clear('Message');

		    return SentEcard::get()->filter('ID',$es->ID)->first();

	    } else {

	    	return SentEcard::get()->byID(111);
	    }
	    
		

	}

	public function ClearSession($id) {

		Session::clear('MyArrayOfValues');

		return Director::baseURL()."ecard-submission?card-id=".$id;
	}


	public function Ecard($id) {
		$e = Ecards::get()->byID($id);
		return $e;
	}

	public function Skin($id) {
		return Skins::get()->byID($id);
	}

	public function Music($id) {
		return Music::get()->byID($id);
	}

	public function Stamp($id) {
		return Stamps::get()->byID($id);
	}

	public function Poem($id) {
		return Poem::get()->byID($id);
	}


	public function MyCard() {
		if($id = $this->request->getVar('card-id'))
		{
			return SentEcard::get()->filter('UID',$id)->first();
		} else {
			return false;
		}
	}
}