<?php

class HomePage extends Page {

	private static $has_many = array(
		"Ecards" => "Ecards",
		"Sliders" => "Slider"
		);

	public function getCMSFields() {

		$fields = parent::getCMSFields();

		$fields->addFieldToTab("Root.MainSlider", GridField::create(
			"Sliders",
			"Home Sliders",
			$this->Sliders(),
			GridFieldConfig_RecordEditor::create()
		));

		return $fields;
	}
}

class HomePage_Controller extends Page_Controller {

	private static $allowed_actions = array (
    );

	public function Category() {

		return Category::get()->limit(3);
		
	}

	public function Ecards() {
		return Ecards::get();
	}

	public function Featured() {
		return Ecards::get()->filter('featured',1)
				->limit(3);
	}

}