<?php

class Music extends DataObject {

	private static $db = array(
		'SongName' => 'Varchar(100)',
		);

	private static $has_one = array(
		"Song" => "File"
		);

	private static $has_many = array(
		"Ecards" => "Ecards"
		);


	public static $summary_fields = array (
		"MusicName" => "SongName"
		);

	function onBeforeWrite() {

		$this->SongName = $this->Song()->Name;

		parent::onBeforeWrite();

	}

	public function MusicName() {

        return $this->Song()->Name;
    
    }

    public function getCMSFields(){

		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main","Name");

		return $fields;

	}

}