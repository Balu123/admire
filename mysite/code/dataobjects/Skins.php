<?php  

/**
*  ecard skins
*/
class Skins extends DataObject
{
	private static $db = array(
		'Name' => 'Varchar(100)',
	);

	private static $has_one = array(
		"Skin" => "Image"
	);

	private static $has_many = array(
		"Ecards" => "Ecards"
		);

	public static $summary_fields = array (
		"getGridThumbnail" => ""
		);

	function onBeforeWrite() {

		$this->Name = $this->Skin()->Name;

		parent::onBeforeWrite();

	}

	public function getGridThumbnail() {
        if($this->Skin()->exists()) {
            return $this->Skin()->SetWidth(100);
        }

        return "(no skin)";
    }

    public function getCMSFields(){

		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main","Name");

		return $fields;	

	}
}