<?php

class Category extends DataObject {
	private static $db = array(
		"Name" => "Varchar(100)"
		);

	private static $belongs_many_many = array(
		'Ecards' => 'Ecards'
		);

	private static $has_one = array(
		"EcardsHolder" => "EcardsHolder"
		);

	
	public function getLink() {
        return EcardsHolder::get()->first()->Link('Category/'.$this->ID);

    }

    public function Count($id) {

	    	$cat = Category::get()->byID($id);
	   	 	$ecards = $cat->Ecards();

	   	// if(!$ecards) {
	    //     return "0";
	    // }

	    return $ecards;
	    }



	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeByName("Main");
		$fields->removeByName("Ecards");
		

	 	$fields->addFieldToTab('Root.Category', TextField::create("Name"));
	 	// $fields->addFieldToTab('Root.Ecard', TextField::create("Price"));

		return $fields;
	}

}