<?php

class Stamps extends DataObject {

	private static $db = array(
		'Name' => 'Varchar(100)',
		);

	private static $has_one = array(
		"Stamp" => "File"
		);

	private static $has_many = array(
		"Ecards" => "Ecards"
		);

	public static $summary_fields = array (
		"getGridThumbnail" => ""
		);
	function onBeforeWrite() {

		$this->Name = $this->Stamp()->Name;

		parent::onBeforeWrite();

	}

	public function getGridThumbnail() {
        if($this->Stamp()->exists()) {
            return $this->Stamp()->SetWidth(100);
        }

        return "(no Stamp)";
    }
    
   public function getCMSFields(){

		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main","Name");

		return $fields;	

	}
}