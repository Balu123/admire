<?php  

class Slider extends DataObject {

	private static $has_one = array(
		"HomePage" => "HomePage",
		"Photo" => "Image"
		);

	public static $summary_fields = array (
		"GridThumbnail" => ""
		);

	public function getGridThumbnail() {
        if($this->Photo()->exists()) {
            return $this->Photo()->SetWidth(100);
        }

        return "(no image)";
    }

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "Photo");
		$fields->removeFieldFromTab("Root.Main", "HomePageID");

		$uploadField = new UploadField("Photo","Slide Image");
		$uploadField->setFolderName('Uploads/slider-images');
		$uploadField->getValidator()->allowedExtensions = array("gif","jpg" ,"jpeg","png");

		$fields->addFieldToTab("Root.Main",$uploadField );

		return $fields;
	}

}