<?php

class SentEcard extends DataObject {

	private static $db = array(
		"EcardID" => "int(100)",
		"UID" => "Varchar(100)",
		"PoemID" => "int(100)",
		"SkinID" => "int(100)",
		"StampID" => "int(100)",
		"MusicID" => "int(100)"
	);

	private static $has_one = array(
		"EcardSubmission" => "EcardSubmission"
		);
}