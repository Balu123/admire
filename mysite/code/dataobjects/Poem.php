<?php

class Poem extends DataObject {

	private static $db = array(
		"Name" => "Varchar(100)",
		"Author" => "Varchar(100)",
		"Content" => "HTMLText"
		);

	private static $has_many = array(
		"Ecards" => "Ecards"
		);

	public static $summary_fields = array (
		"Name" => "Name"
		);

}