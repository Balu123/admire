<?php  

class Ecards extends DataObject {

	private static $db = array(
		"Name" => "Varchar(100)",
		"Price" => "Int(10)",
		"Category" => "Varchar(100)",
		"Type" => "Int(5)"	,
		"Featured" => "Boolean"
		);

	private static $has_one = array(
		"HomePage" => "HomePage",
		"Photo" => "Image",
		'Poem' => 'Poem',
		'Music' => 'Music',
		'Skins' => 'Skins',
		'Stamps' => 'Stamps',
		'Fav' => 'Fav'
		);

	private static $many_many = array(
		'Category' => 'Category',
		"Ratings" => "Rating"
	);

	// private static $belongs_many_many = array(
	// 	"MyMemberExtension" => "MyMemberExtension"
	// 	);

	public static $summary_fields = array (
		"Name" => "Name"
		);

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeByName("Main");
		$fields->removeByName("Ratings");
		$fields->removeByName("Category");
		$uploadField = new UploadField("Photo");
		$uploadField->setFolderName('Uploads/ecard-images');
		$uploadField->getValidator()->allowedExtensions = array("gif","jpg" ,"jpeg","png");
		$fields->addFieldToTab("Root.Ecard",$uploadField );

		$fields->addFieldToTab('Root.Ecard', new ListboxField(
			'Category', // Name of has_many (or many_many) relation
			'Category', // Title
			Category::get()->map('ID', 'Name')->toArray(), // Get source objects
			$this->Category()->column('ID'), // Get ids of currectly selected objects
			null,
			true	// Means make this a multi-select field.
		));

	 	$fields->addFieldToTab('Root.Ecard', TextField::create("Name"));
	 	$fields->addFieldToTab('Root.Ecard', TextField::create("Price","Pay per card"));
	 	$fields->addFieldToTab('Root.Ecard', OptionsetField::create(
		   $name = "Type",
		   $title = "Select who can send ecard",
		   $source = array(
		      "1" => "Guest",
		      "2" => " Free Basic membership"
		   ),
		   $value = "1"
		));

	 	$fields->addFieldToTab('Root.Ecard', DropdownField::create('PoemID', 'Poem', Poem::get()->map('ID', 'Name'))
                ->setEmptyString('(Select one)'));

	 	$fields->addFieldToTab('Root.Ecard', DropdownField::create('MusicID', 'Music', Music::get()->map('ID', 'Name'))
                ->setEmptyString('(Select one)'));

		$fields->addFieldToTab('Root.Ecard', DropdownField::create('SkinsID', 'Skins', Skins::get()->map('ID', 'Name'))
		                ->setEmptyString('(Select one)'));

		$fields->addFieldToTab('Root.Ecard', DropdownField::create('StampsID', 'Stamps', Stamps::get()->map('ID', 'Stamp'))
		                ->setEmptyString('(Select one)'));

		$fields->addFieldToTab('Root.Ecard', CheckboxField::create('Featured'));

		return $fields;
	}

	public function getLink() {
	        return EcardsHolder::get()->first()->Link('Show/'.$this->ID);

	}

	public function CategoriesList() {
        if($this->Category()->exists()) {
            return implode(', ', $this->Category()->column('Name'));
        }
    }

    public function Categories($id) {
    	$str = "";
	    	$ecard = Ecards::get()->byID($id);
	    	$cat = $ecard->Category();
	    	// foreach ($cat as $value) {
	    	// 	$str = $value->Name;
	    	// }
	    	return $cat;
    }

    public function Poems($id) {

	    	$p = Ecards::get()->byID($id);
	   	 	$pm = $p->Poem();

	    return $pm;
	}

	public function Musics($id) {

	    	$p = Ecards::get()->byID($id);
	   	 	$pm = $p->Music();

	    return $pm;
	}

	public function fav() {
		return "test route";
	}

}