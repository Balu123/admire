<?php

class IPBlocker extends DataObject {

	private static $db = array(
		"IPAdress" => "Varchar(100)",
		"UserName" => "Varchar(100)",
		"Block" => "enum('unblock,block')",		
		);

	private static $summary_fields = array(
		"UserName" => "UserName"
		);
}