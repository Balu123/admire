	<div id="content" class="site-content">
		<div class="container content-wrapper">
			
	<div id="primary" class="content-area"><main id="main" class="site-main" role="main"><nav class="woocommerce-breadcrumb" ><a href="$BaseHref">Home</a>&nbsp;&#47;&nbsp;$Query</nav>
		<h3 class="woo-title entry-title">$URLSegment</h3>
		
<ul class="products">
<% if $Results %>	
<p>Showing all $Results.count results for $Query</p>							
<% loop $Results %>
	<li class="first post-46 product type-product status-publish has-post-thumbnail product_cat-birthday product_tag-birthday product_tag-party clearfix featured virtual shipping-taxable purchasable product-type-simple product-cat-birthday product-tag-birthday product-tag-party instock">
	<a href="$Link"><img width="300" height="236" src="$Photo.URL" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="$Photo.URL" /><h3>$Name </h3>
<% if $Featured %>Featured<% end_if %>
	<span class="price"><span class="amount"><% if $Price %>&#36;$Price<% end_if %></span></span>
</a>
<div class="social-share">
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_linkedin_large' displayText='LinkedIn'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>
</div>
</li>
<% end_loop %>
<% end_if %>
</ul>

<ul class="products">
<% if $Ecards %>	
<p>Showing all $Ecards.count results for $Query</p>										
<% loop $Ecards %>
	<li class="first post-46 product type-product status-publish has-post-thumbnail product_cat-birthday product_tag-birthday product_tag-party clearfix featured virtual shipping-taxable purchasable product-type-simple product-cat-birthday product-tag-birthday product-tag-party instock">
	<a href="$Link"><img width="300" height="236" src="$Photo.URL" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="$Photo.URL" /><h3>$Name </h3>
<% if $Featured %>Featured<% end_if %>
	<span class="price"><span class="amount"><% if $Price %>&#36;$Price<% end_if %></span></span>
</a>
<div class="social-share">
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_linkedin_large' displayText='LinkedIn'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>
</div>
</li>
<% end_loop %>
<% end_if %>
</ul>
<% if not $Results.count && not $Ecards.count %>
<p>No results found for $Query</p>
<% end_if %>
	</main></div>
	

		</div>
	</div><!-- #content -->