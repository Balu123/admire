	<div id="content" class="site-content">
		<div class="container content-wrapper">
			
	<div id="primary" class="content-area"><main id="main" class="site-main" role="main"><nav class="woocommerce-breadcrumb" ><a href="../index.html">Home</a>&nbsp;&#47;&nbsp;$Title</nav>
		<h3 class="woo-title entry-title">Shop</h3>
		
		
			<div class="before-shop clearfix"><p class="woocommerce-result-count">
	Showing all $Ecards.count results</p>
<form class="woocommerce-ordering" method="get">
	<select name="	orderby" class="orderby">
					<option value="menu_order"  selected='selected'>Default sorting</option>
					<option value="popularity" >Sort by popularity</option>
					<option value="rating" >Sort by average rating</option>
					<option value="date" >Sort by newness</option>
					<option value="price" >Sort by price: low to high</option>
					<option value="price-desc" >Sort by price: high to low</option>
			</select>
	</form>
</div>
			<ul class="products">

<% if CurrentMember %>
<% if $Fav %>				
<% loop $Fav %>				
<li class="first post-46 product type-product status-publish has-post-thumbnail product_cat-birthday product_tag-birthday product_tag-party clearfix featured virtual shipping-taxable purchasable product-type-simple product-cat-birthday product-tag-birthday product-tag-party instock">
<% if $Featured %>Featured<% end_if %>
	<a href="$Link"><img width="300" height="236" src="$Photo.URL" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="balls-313404_1280" /><h3>$Name </h3>

	<span class="price"><span class="amount"><% if $Price %>&#36;$Price<% end_if %></span></span>
</a>
<!-- <p>Rating: ***</p> -->
<br/>
<a class="button_link_style1" id="add_to_fav" href="favorites/?delfav=$ID">Remove from favorites</a>
</li>
<p><% with $Musics($ID) %>$Title<% end_with %></p>
<% end_loop %>
<% else %>
<p>No favorite Ecard found.</p>
<% end_if %>
<% else %>
<p>you need to be logged in to make your favourite ecards. </p>
<% end_if %>


			</ul>

			
		
	</main></div>
	

		</div>
	</div><!-- #content -->