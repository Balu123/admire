$isBlocked
<!DOCTYPE html>
<html lang="en-US">
   <!-- Mirrored from magic-ecards.greataussiewebdesigns.com.au/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 11 Jun 2016 03:07:44 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <!-- /Added by HTTrack -->
   <head>
   <% base_tag %>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="xmlrpc.php">
      <title><% if $MetaTitle %>$MetaTitle<% else %>$MenuTitle<% end_if %> | $SiteConfig.Title</title>
      <link rel="alternate" type="application/rss+xml" title="Magic Ecards &raquo; Feed" href="feed/index.html" />
      <link rel="alternate" type="application/rss+xml" title="Magic Ecards &raquo; Comments Feed" href="comments/feed/index.html" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script type='text/javascript' src='$ThemeDir/js/jquerycad0.js?ver=1.12.3'></script>
      <script type='text/javascript' src='$ThemeDir/js/jquery-migrate.min2fca.js?ver=1.4.0'></script>
      <script type='text/javascript' src='$ThemeDir/js/facebook-embed.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/wp-embed.min3428.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/cart-fragments.min4468.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/add-to-cart.min4468.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/jquery.blockUI.min44fd.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/woocommerce.min4468.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/jquery.cookie.min330a.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/wpgroho3428.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/skip-link-focus-fix08e0.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/main.min3428.js'></script>
      <script type='text/javascript' src='$ThemeDir/js/scripts.min3428.js'></script>

<script type='text/javascript' src='$ThemeDir/js/address-i18n.min4468.js'></script><script type='text/javascript' src='$ThemeDir/js/comment-reply.min3428.js'></script><script type='text/javascript' src='$ThemeDir/js/country-select.min4468.js'></script><script type='text/javascript' src='$ThemeDir/js/jquery.prettyPhoto.init.min4468.js'></script><script type='text/javascript' src='$ThemeDir/js/jquery.prettyPhoto.min005e.js'></script><script type='text/javascript' src='$ThemeDir/js/lost-password.min4468.js'></script><script type='text/javascript' src='$ThemeDir/js/password-strength-meter.min4468.js'></script><script type='text/javascript' src='$ThemeDir/js/select2.mine248.js'></script><script type='text/javascript' src='$ThemeDir/js/single-product.min4468.js'></script><script type='text/javascript' src='$ThemeDir/js/zxcvbn-async.min5152.js'></script>
<script src="$ThemeDir/js/jquery.dd.min.js"></script>
<script src="$ThemeDir/js/rating.js"></script>
<script src="$ThemeDir/js/main.js"></script>
<script src="$ThemeDir/js/jquery.elevatezoom.js"></script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "924c463e-cf37-4af9-8426-1fe1694887c3", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/magic-ecards.greataussiewebdesigns.com.au\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.2"}};
         !function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         .search-bar {
            display: none;
         }
      </style>
      <script type="text/javascript">
         jQuery(document).ready(function(e) {
         
         // jQuery('.search-bar').hide();
 jQuery("#srch").click(function() {  
            jQuery(".search-bar").toggle(400);          
         });
         jQuery("#close").click(function(){
            jQuery("#boxes").hide();
         });
        
         //jQuery('img').magnify();
             // jQuery(".products img").elevateZoom({tint:true, tintColour:'#F90', tintOpacity:0.5,zoomWindowPosition: 5, zoomWindowOffetx:         10,zoomWindowWidth:300,zoomWindowHeight:300});

            jQuery("#skin").msDropdown();
            jQuery("#poem").msDropdown();
            jQuery("#stamps").msDropdown();
            jQuery("#music").msDropdown();
            jQuery("#ForgotPassword").wrap("<div class='forgot-pass'></div>");

         });
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <% if $CurrentMember %>
      <style type="text/css">
      .sign-up {
         display: none;
      }
      </style>
      <% end_if %>
      <link rel='stylesheet' id='theshop-bootstrap-css'  href='$ThemeDir/css/bootstrap.min68b3.css' type='text/css' media='all' />
      <link rel='stylesheet' id='theshop-wc-css-css'  href='$ThemeDir/css/wc.min3428.css' type='text/css' media='all' />
      <link rel='stylesheet' id='woocommerce-layout-css'  href='$ThemeDir/css/woocommerce-layout4468.css' type='text/css' media='all' />
      <link rel='stylesheet' id='woocommerce-smallscreen-css'  href='$ThemeDir/css/woocommerce-smallscreen4468.css' type='text/css' media='only screen and (max-width: 768px)' />
      <link rel='stylesheet' id='theshop-style-css'  href='$ThemeDir/css/style3428.css' type='text/css' media='all' />
      <link rel='stylesheet' id='theshop-body-fonts-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A400%2C500%2C600%2C700&amp;ver=4.5.2' type='text/css' media='all' />
      <link rel='stylesheet' id='theshop-headings-fonts-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A300%2C400%2C700&amp;ver=4.5.2' type='text/css' media='all' />
      <link rel='stylesheet' id='theshop-fontawesome-css'  href='$ThemeDir/css/font-awesome.min3428.css' type='text/css' media='all' />
      <link rel='stylesheet' id='jetpack_css-css'  href='$ThemeDir/css/jetpackfa0c.css' type='text/css' media='all' />
      <link rel="stylesheet" href="$ThemeDir/css/sample.css" />
<link rel='stylesheet' id='genericons-css'  href='$ThemeDir/css/genericons128b.css' type='text/css' media='all' />

<link rel='stylesheet' id='genericons-css'  href='$ThemeDir/css/prettyPhoto4468.css' type='text/css' media='all' />

<link rel='stylesheet' id='genericons-css'  href='$ThemeDir/css/select24468.css' type='text/css' media='all' />
<link rel="stylesheet" type="text/css" href="$ThemeDir/css/dd.css">
<link rel="stylesheet" type="text/css" href="$ThemeDir/css/rating.css">
<link rel="stylesheet" type="text/css" href="$ThemeDir/css/main.css">
<link rel="stylesheet" type="text/css" href="$ThemeDir/css/magnify.css">

      <style id='theshop-style-inline-css' type='text/css'>
         .site-logo { max-height:110px; }
         .site-header { padding:30px 0; }
         .products-loop { background-color:#8169b5}
         .cta-section { background-color:#2C292A}
         .cats-loop { background-color:#f76262}
         .posts-loop { background-color:#ffffff}
         .products-loop, .products-loop .section-title, .products-loop h3, .products-loop .woocommerce ul.products li.product .price { color:#000000}
         .cta-section { color:#fff}
         .cats-loop, .cats-loop .section-title { color:#000000}
         .posts-loop, .posts-loop .section-title, .posts-loop .post-title a { color:#000000}
         .woocommerce #respond input#submit,.woocommerce a.button,.woocommerce button.button,.woocommerce input.button, .woocommerce div.product p.price,.woocommerce div.product span.price,.woocommerce .woocommerce-info:before,.woocommerce .woocommerce-message:before,.woocommerce .woocommerce-message:before,.preloader .preloader-inner,.entry-title a:hover,.woocommerce .star-rating span,a, a:hover, .main-navigation a:hover { color:#1e73be}
         .add_to_cart_button::before,.cart-button::before,.woocommerce .widget_price_filter .ui-slider .ui-slider-range,.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,.woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover,.woocommerce span.onsale,.owl-theme .owl-controls .owl-page span,li.nav-cart,.widget-title::after,.post-navigation a,.posts-navigation a,.secondary-navigation li:hover,.secondary-navigation ul ul,button, .button, input[type="button"], input[type="reset"], input[type="submit"] { background-color:#1e73be}
         .woocommerce .woocommerce-info,.woocommerce .woocommerce-message,.woocommerce .woocommerce-error,.woocommerce .woocommerce-info,.woocommerce .woocommerce-message,.main-navigation ul ul { border-top-color:#1e73be;}
         .woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover { border-color:#1e73be;}
         body, .widget a { color:#4c4c4c}
         .site-header { background-color:#dd3333}
         .main-navigation a { color:#1c1c1c}
         .site-title a, .site-title a:hover { color:#ffffff}
         .site-description { color:#000000}
         body, .main-navigation ul ul li { font-family:'Raleway', serif;}
         h1, h2, h3, h4, h5, h6, .main-navigation li, .promo-box span { font-family:'Raleway', sans-serif;}
         .site-title { font-size:47px; }
         .site-description { font-size:21px; }
         h1 { font-size:33px; }
         h2 { font-size:34px; }
         h3 { font-size:26px; }
         h4 { font-size:22px; }
         h5 { font-size:17px; }
         h6 { font-size:15px; }
         body { font-size:18px; }
      </style>
      
      <!-- Inline jetpack_facebook_likebox -->
      <style id='jetpack_facebook_likebox-inline-css' type='text/css'>
         .widget_facebook_likebox {
         overflow: hidden;
         }
      </style>

      <link rel='https://api.w.org/' href='wp-json/index.html' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
      <meta name="generator" content="WordPress 4.5.2" />
      <meta name="generator" content="WooCommerce 2.5.5" />
      <link rel="canonical" href="index.html" />
      <link rel='shortlink' href='http://wp.me/P7Aj9x-h' />
      <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed3ed8.json?url=http%3A%2F%2Fmagic-ecards.greataussiewebdesigns.com.au%2F" />
      <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed6113?url=http%3A%2F%2Fmagic-ecards.greataussiewebdesigns.com.au%2F&amp;format=xml" />
      <link rel='dns-prefetch' href='http://v0.wordpress.com/'>
      <style type='text/css'>img#wpstats{display:none}</style>
      <!--[if lt IE 9]>
      <script src="http://magic-ecards.greataussiewebdesigns.com.au/wp-content/themes/theshop/js/html5shiv.js"></script>
      <![endif]-->
      <style type="text/css" id="custom-background-css">
         body.custom-background { background-image: url('$ThemeDir/images/fantasy-1390177.jpg'); background-repeat: no-repeat; background-position: top center; background-attachment: fixed; }
      </style>
      <!-- Jetpack Open Graph Tags -->
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Magic Ecards" />
      <meta property="og:description" content="Buy, Sell and Share your favorite Ecards" />
      <meta property="og:site_name" content="Magic Ecards" />
      <meta property="og:locale" content="en_US" />
      <meta name="twitter:card" content="summary" />
      <style id="custom-css-css">.site-footer,.site-footer a{display:none}.footer-widgets{background-color:#000;border-top:1px solid #f1f1f1;padding:30px 0}ul,ol{list-style:none}li{text-decoration:none;color:#fff}.main-navigation a{color:#fff;background:#000;border-radius:5px;padding:10px 13px}.main-navigation a:hover{color:#fff;background:#1E73BE;border-radius:5px;padding:10px 13px}.secondary-navigation{background-color:#000;width:25%;float:left}.woocommerce a.button{color:#fff}.woocommerce ul.products li.product a img{width:100%;height:auto;display:block;margin:0;box-shadow:none;max-height:171px}.content-wrapper{background-color:rgba(255,255,255,0.5)}.products-loop{background-color:#8169b5;border:2px solid #000;padding:20px}li.nav-cart{background:rgba(0,0,0,0) !important}.owl-theme .owl-controls .owl-buttons div{color:#FFF;display:none;zoom:1;font-size:52px;filter:Alpha(Opacity=70);opacity:.7;position:absolute;top:45%}.owl-carousel .owl-item{float:left;max-height:450px}.cats-loop{border:2px solid #000;padding:70px 0 0}ul.products{margin:0 0 1em;padding-left:30px;list-style:none;clear:both}.cta-section .button{border-radius:5px}.woocommerce .woocommerce-breadcrumb{background-color:rgba(0,0,0,0)}.woocommerce .woocommerce-breadcrumb a{color:#fff;text-decoration:none}nav#secondary-nav li {list-style: none;}  </style>
   </head>
   <body class="home page page-id-17 page-template page-template-page-templates page-template-page_front-page page-template-page-templatespage_front-page-php custom-background fullwidth-single">
      <div class="preloader">
       <div class="preloader-inner">
        <h1>LOADING...</h1>
            <img src="http://dyam.greataussiewebdesigns.com.au/themes/ecards/images/ajax_loader_red_256.gif">   
        </div> 
      </div>
      <% include Header %>
      <!-- #content -->
      $Layout
      <% include Footer %>
      <!-- #colophon -->
      </div><!-- #page -->
      <div style="display:none">
      </div>
      
      <script type='text/javascript'>
         /* <![CDATA[ */
         var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View Cart","cart_url":"http:\/\/magic-ecards.greataussiewebdesigns.com.au\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
         /* ]]> */
      </script>
     
      <script type='text/javascript'>
         /* <![CDATA[ */
         var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
         /* ]]> */
      </script>

      <script type='text/javascript'>
         /* <![CDATA[ */
         var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
         /* ]]> */
      </script>

      <script type='text/javascript'>
         /* <![CDATA[ */
         var WPGroHo = {"my_hash":""};
         /* ]]> */
      </script>

      <script type='text/javascript'>
         /* <![CDATA[ */
         var jpfbembed = {"appid":"249643311490","locale":"en_US"};
         /* ]]> */
      </script>

      <script type='text/javascript'>
         _stq = window._stq || [];
         _stq.push([ 'view', {v:'ext',j:'1:4.0.3',blog:'112087787',post:'17',tz:'0',srv:'magic-ecards.greataussiewebdesigns.com.au'} ]);
         _stq.push([ 'clickTrackerInit', '112087787', '17' ]);
      </script>
   </body>
   <!-- Mirrored from magic-ecards.greataussiewebdesigns.com.au/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 11 Jun 2016 03:09:55 GMT -->
</html>