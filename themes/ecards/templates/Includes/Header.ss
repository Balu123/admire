      <div id="page" class="hfeed site">
         <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
         <header id="masthead" class="site-header clearfix" role="banner">
            <div class="container">
               <div class="site-branding col-md-4">
                  <a href="index.html" title="Magic Ecards"><img class="site-logo" src="$ThemeDir/images/johnny-automatic-devil-in-top-hat.png" alt="Magic Ecards" /></a>			
               </div>
               <!-- .site-branding -->
            <% include MainNav %>
               <!-- #site-navigation -->
               <nav class="mobile-nav"></nav>
            </div>
         </header>
         <!-- #masthead -->
         <div class="header-promo">
            <div class="container">
               <div class="promo-box"><i class="fa fa-rocket"></i><span>No shipping</span></div>
               <div class="promo-box"><i class="fa fa-money"></i><span>Best prices</span></div>
               <div class="promo-box"><i class="fa fa-clock-o"></i><span>Instant Share</span></div>
            </div>
         </div>

      </div>